//  Setup the modules/dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js"); // para connect sa taskrRoutes/ connected na tdin si app.js sa lahat(domino effect)

// Server Setup
const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));//para ma translate all data to express
// Add the task route
// allows all the task routes created in the taskRoutes.js file to use / tasks route.

app.use("/tasks", taskRoutes); // location kung saan ng galit yung /task: default endpoint/ para maging katabi nya lahat ng ibang end points: proper seperation sa ibang endpoint

// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.mr7hck1.mongodb.net/B229_to-do?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true,
});

let db = mongoose.connection; // server para kay mongodb

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}.`));
