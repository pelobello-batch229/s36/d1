// ma iinherit parin ni route yung app.js 
const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js");// para maconnect sa taskContronllers.js

// [Section] - Routes(Humahawak ng mga endpoints)
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

// Route to get all task
router.get("/", (req,res) =>{//use to seperate yung mga endpoints
	// kahit anong name "taskController"/to gain access to the taskControllers.js get all task
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController))

});

// Creating a new task
router.post("/", (req,res) =>{
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// DELETE task wildcard(:id) // req.params.id(why params? url id is called params)
router.delete("/:id", (req,res) =>{
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Update Task
router.put("/:id", (req,res) =>{
	taskControllers.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
})

// Get specific Task
router.get("/:id", (req,res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Update status to "complete"
router.put("/:id/complete", (req,res) => {
	taskControllers.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})



module.exports = router; // mga codes nasa loob nung files na ito ay  binibigyan ng natin ng access ang ibang files para magamit yung codes dito



